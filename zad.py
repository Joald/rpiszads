#IMPORTS
import numpy as np
import random as rd
import matplotlib.pyplot as plt
stats = np.loadtxt('us_births_69_88.csv', dtype=int, delimiter=',', skiprows = 1)
D = stats.shape[0]
p = stats[:,2] / np.sum(stats[:,2])
pmax = np.max(p)

# FAST PROCEDURES
def processRand(number, uniform, res, n):
    if uniform < p[number] / pmax:
        res.append(number)
    if len(res) == n:
        return res
    
def randDayN(n): 
    res = []
    while True:
        rands = np.random.randint(0, high=D-1, size=n)
        uniforms = np.random.uniform(size=n)
        for r, u in zip(rands, uniforms):
            processRand(r, u, res, n)
            if len(res) == n:
                return res

def processDays(n, res, days):
    s = set()
    counter = 0
    for day in days:
        if day in s:
            res.append(len(s) + 1)
            s = set()
        s.add(day)
        if len(res) == n:
            return

def randKFast(n):
    res = []
    while len(res) < n:
        days = randDayN(n)
        processDays(n, res, days)
    return res

# EXECUTE
sample = randKFast(100000)
plt.hist(sample, bins=range(1,101))
plt.show()
